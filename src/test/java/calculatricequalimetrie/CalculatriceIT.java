/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatricequalimetrie;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ndimb
 */
public class CalculatriceIT {
    
    public CalculatriceIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class Calculatrice.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Calculatrice expResult = Calculatrice.getInstance();
        Calculatrice result = Calculatrice.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        int a = 2;
        int b = 1;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 3;
        int result = instance.addition(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        int a = 0;
        int b = 0;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 0;
        int result = instance.soustraction(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double a = 30.0;
        double b = 2.0;
        Calculatrice instance = Calculatrice.getInstance();
        double expResult = 15.0;
        double result = instance.division(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        int a = 1;
        int b = 1;
        Calculatrice instance = Calculatrice.getInstance();
        int expResult = 1;
        int result = instance.multiplication(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of calculMoyenne method, of class Calculatrice.
     */
    @Test
    public void testCalculMoyenne() {
        System.out.println("calculMoyenne");
        List<Integer> nombres = Arrays.asList(3, 4, 9, 8, 15);
        Calculatrice instance = Calculatrice.getInstance();
        double expResult = 7.8;
        double result = instance.calculMoyenne(nombres);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
