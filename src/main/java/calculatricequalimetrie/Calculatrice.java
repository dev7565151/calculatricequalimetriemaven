/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatricequalimetrie;

import java.util.List;

/**
 *
 * @author ndimb
 */
public class Calculatrice {
    private static Calculatrice instance;

    Calculatrice() {

    }

    public static Calculatrice getInstance() {
        if (instance == null) {
            instance = new Calculatrice();
        }
        return instance;
    }

    public int addition(int a, int b) {
        return a + b;
    }

    public int soustraction(int a, int b) {
        return a - b;
    }

    public double division(double a, double b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("Attention division par zéro !");
        }
    }

    public int multiplication(int a, int b) {
        return a * b;
    }

    public double calculMoyenne(List<Integer> nombres) {
        if (nombres.isEmpty()) {
            throw new IllegalArgumentException("Liste de nombres vide veuillez remplir la liste!");
        }

        int somme = 0;
        for (int nombre : nombres) {
            somme += nombre;
        }
        return (double) somme / nombres.size();
    }
}
