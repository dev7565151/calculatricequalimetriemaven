/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatricequalimetrie;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ndimb
 */
public class CalculatriceQualimetrie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Calculatrice calculatrice = Calculatrice.getInstance();

        System.out.println("Addition : " + calculatrice.addition(10, 3));
        System.out.println("Soustraction : " + calculatrice.soustraction(5, 6));
        System.out.println("Division : " + calculatrice.division(10, 3));
        System.out.println("Multiplication : " + calculatrice.multiplication(5, 2));

        List<Integer> nombres = new ArrayList<>();
        nombres.add(3);
        nombres.add(4);
        nombres.add(9);
        nombres.add(8);
        nombres.add(15);
        System.out.println("Moyenne : " + calculatrice.calculMoyenne(nombres));
    }
}
